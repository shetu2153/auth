import base64, json
class SimpleMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response
        # One-time configuration and initialization.

    def __call__(self, request):
        # Code to be executed for each request before
        # the view (and later middleware) are called.
        if 'HTTP_AUTHORIZATION' in request.META:
            payload = request.META['HTTP_AUTHORIZATION'].split('.')[1].strip()
            data = json.loads(base64.b64decode(payload))
            request.user_json = data
        response = self.get_response(request)

        # Code to be executed for each request/response after
        # the view is called.

        return response