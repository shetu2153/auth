from django.urls import path
from . import views
urlpatterns = [
    path('generate', views.TokenGenerationApiView.as_view(), name='token_generation_api_view'),
    path('home', views.AuthHomeApiView.as_view(), name='auth_home_api_view')
]