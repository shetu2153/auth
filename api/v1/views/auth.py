from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from django.contrib.auth import get_user_model
from django.contrib.auth.models import User
from jwcrypto import jwt as jwk
import python_jwt as jwt
from django.conf import settings
import datetime
import base64


class TokenGenerationApiView(APIView):
    def post(self, request):
        try:
            username = request.data.get('username', None)
            password = request.data.get('password', None)
            print(username, password)
            if not username:
                return Response(
                    dict(
                        status=400,
                        message='username is required'
                    ), status=status.HTTP_400_BAD_REQUEST
                )
            if not password:
                return Response(
                    dict(
                        status=400,
                        message='password is required'
                    ), status=status.HTTP_400_BAD_REQUEST
                )
            data = dict()
            if username and password:
                u: User = get_user_model().objects.filter(
                    username=username
                ).first()
                if not u:
                    return Response(
                        dict(
                            status=404,
                            message='User not found'
                        ), status=status.HTTP_404_NOT_FOUND
                    )
                if not u.check_password(password):
                    return Response(
                        dict(
                            status=403,
                            message='Password not matched'
                        ), status=status.HTTP_403_FORBIDDEN
                    )
                # generate token
                payload = dict(
                    iss='auth-issuer',
                    sub=u.username,
                )
                token = jwt.generate_jwt(
                    payload, priv_key=jwk.JWK.from_json(getattr(settings, 'PRIVATE_KEY')), algorithm='RS256', lifetime=datetime.timedelta(hours=1)
                )
                data = dict(
                    token=token
                )
            return Response(
                data, status=status.HTTP_200_OK
            )
        except Exception as exc:
            return Response(
                dict(
                    status=500,
                    message=str(exc)
                ), status=status.HTTP_500_INTERNAL_SERVER_ERROR
            )
import json
class AuthHomeApiView(APIView):
    def get(self, request):
        try:
            data = dict(
                from_middleware=request.user_json
            )
            return Response(data, status=status.HTTP_200_OK)
        except Exception as exc:
            return Response(
                dict(
                    status=500,
                    message=str(exc)
                ),
                status=status.HTTP_500_INTERNAL_SERVER_ERROR
            )