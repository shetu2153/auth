token = 'test-token'
public_key='pub-key'
import python_jwt as jwt, jwcrypto.jwt as jwk


header, claims = jwt.verify_jwt(token, jwk.JWK.from_json(public_key), ['RS256'])
print(header, claims)