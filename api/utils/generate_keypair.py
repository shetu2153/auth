import jwcrypto.jwt as jwk

key = jwk.JWK.generate(kty='RSA', size=2048)
public_key = key.export_public()
private_key = key.export_private()

print(public_key)
print(private_key)